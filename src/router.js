import Vue from 'vue'
import VueRouter from 'vue-router'

import Cart from './app/cart'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/cart', component: Cart },
    { path: '*', redirect: '/cart' },
  ],
})

export default router
