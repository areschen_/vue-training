import Vue from 'vue'
import Vuex from 'vuex'

import ui from './modules/ui'
import request from './modules/request'
import test from './modules/test'
import weixin from './modules/weixin'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    ui,
    request,
    weixin,
    test,
  },
})

if (module.hot) {
  // 使 modules 成为可热重载模块
  module.hot.accept([
    './modules/ui',
    './modules/request',
    './modules/weixin',
    './modules/test',
  ], () => {
    store.hotUpdate({
      modules: {
        ui: require('./modules/ui').default,
        request: require('./modules/request').default,
        weixin: require('./modules/weixin').default,
        test: require('./modules/test').default,
      },
    })
  })
}

export default store
