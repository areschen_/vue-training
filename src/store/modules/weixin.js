import { actionCreator, mutationCreator } from 'vue-actions'
import Weixin from '../../components/weixin'

const state = {
  siteInfo: {},
  shouldShowShare: false,
}

const getters = {
  siteInfo: state => state.siteInfo,
  shouldShowShare: state => state.shouldShowShare,
}

export const actions = {
  toggleShare: actionCreator('TOGGLE_SHARE'),
  registerShare: actionCreator('REGISTER_SHARE', ({ commit }, config) => {
    Weixin.registerShare(config, () => {
    })
    return config
  }),
}

export const mutations = mutationCreator((on) => {
  on(actions.toggleShare, (state, bool) => {
    state.shouldShowShare = bool
  })
  on(actions.registerShare, (state, siteInfo) => {
    state.siteInfo = siteInfo
  })
})

export default {
  state,
  getters,
  actions,
  mutations,
}
