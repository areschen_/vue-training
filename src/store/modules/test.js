import { actionCreator, mutationCreator } from 'vue-actions'

const state = {
  shouldShowButton: true,
  progress: 0,
}

export const getters = {
  progress: state => state.progress,
}

export const actions = {
  setProgress: actionCreator('setProgress', function ({ commit }) {
    const actionName = this.successActionName
    return new Promise((resolve) => {
      window.setTimeout(() => {
        resolve(40)
      }, 1000)
      window.setTimeout(() => {
        commit(actionName, 60)
      }, 2000)
      window.setTimeout(() => {
        commit(actionName, 100)
      }, 3000)
    })
  }),
}

export const mutations = mutationCreator((on) => {
  on(actions.setProgress, (state, sssss) => {
    state.progress = sssss
  })

  on.success(actions.setProgress, (state, sssss) => {
    console.log('setProgress success', sssss)
    state.progress = sssss
  })
})

export default {
  state,
  getters,
  actions,
  mutations,
}
