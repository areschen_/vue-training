import { actionCreator, mutationCreator } from 'vue-actions'
import { reduce } from 'lodash'

const state = {
  data: {},
  total: 0,
}

const getters = {
  requestTotal: state => state.total,
}

export const actions = {
  requestStart: actionCreator('REQUEST_START'),
  requestFinish: actionCreator('REQUEST_FINISH'),
}

export const mutations = mutationCreator((on) => {
  const calculate = function (state) {
    state.total = reduce(state.data, (prev, item) => {
      return prev + item
    }, 0)
  }

  on(actions.requestStart, (state, request) => {
    if (request.params.hideToast) {
      return false
    }
    const key = `${request.method}-${request.url}`
    const count = state.data[key] || 0
    state.data[key] = count + 1
    calculate(state)
  })

  on(actions.requestFinish, (state, request) => {
    if (request.params.hideToast) {
      return false
    }
    const key = `${request.method}-${request.url}`
    const count = state.data[key] || 1
    state.data[key] = count - 1
    calculate(state)
  })
})

export default {
  state,
  getters,
  actions,
  mutations,
}
