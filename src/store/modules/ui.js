import { actionCreator, mutationCreator } from 'vue-actions'
import { isString } from 'lodash'
import TOAST_TYPE from '../../constants/toast_type'

const state = {
  shouldShowToast: false,
  toast: {},
}

const getters = {
  shouldShowToast: state => state.shouldShowToast,
  uiToast: state => state.toast,
}

export const actions = {
  showToast: actionCreator('SHOW_TOAST', ({ commit }, payload) => {
    const config = Object.assign(isString(payload) ? {
      text: payload,
    } : payload, {
      timeout: 2500,
      type: TOAST_TYPE.TEXT,
    })
    const hideToast = actions.hideToast.toString()
    if (config.timeout) {
      window.setTimeout(function () {
        commit(hideToast)
      }, config.timeout)
    }
    return config
  }),
  hideToast: actionCreator('HIDE_TOAST'),
  showLoading: actionCreator('SHOW_LOADING', ({ commit }, payload) => {
    return {
      type: TOAST_TYPE.LOADING,
      text: payload.text || '数据加载中',
    }
  }),
}

export const mutations = mutationCreator((on) => {
  on(actions.showToast, (state, config) => {
    state.toast = config
    state.shouldShowToast = true
  })

  on(actions.hideToast, (state) => {
    state.shouldShowToast = false
  })
})

export default {
  state,
  getters,
  actions,
  mutations,
}
