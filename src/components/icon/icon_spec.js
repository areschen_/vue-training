import Vue from 'vue'
import Icon from './index.vue'

function renderComponent (Component, propsData) {
  const Ctor = Vue.extend(Component)
  const vm = new Ctor({ propsData }).$mount()
  return vm.$el
}

describe('test icon.vue', () => {
  it('should generate correct class name', () => {
    const iconName = 'wechat'
    const icon = renderComponent(Icon,{ icon: iconName})
    expect(icon.className).toEqual(`ui-icon-${iconName}`)
  })
})
