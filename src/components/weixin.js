import Cookie from 'js-cookie'
import { assign } from 'lodash'

export default {
  getOpenId () {
    const openId = Cookie.get('openId')
    if (openId) {
      return openId
    }
  },
  openInWeixin () {
    return /MicroMessenger/i.test(navigator.userAgent)
  },
  ready: function (fn) {
    if (this.openInWeixin()) {
      return wx.ready(fn)
    } else {
      window.setTimeout(fn, 500)
    }
  },
  getLocation: function () {
    const promise = new Promise((resolve, reject) => {
      if (this.openInWeixin()) {
        this.ready(function () {
          wx.getLocation({
            success: function (res) {
              resolve(res.res ? res.res : res)
            },
            error: function (res) {
              reject(res)
            },
            cancel: function (res) {
              reject(res)
            },
          })
        })
      } else {
        var defaultOptions = {
          maximumAge: 1000 * 60 * 10,
          timeout: 15000,
          enableHighAccuracy: false,
        }
        navigator.geolocation.getCurrentPosition(function (position) {
          resolve(position.coords)
        }, function (error) {
          reject(error)
        }, defaultOptions)
      }
    })
    return promise
  },
  config: function (configInfo, debug = true) {
    wx.config(
      {
        ...configInfo,
        debug,
        jsApiList: [
          'checkJsApi',
          'onMenuShareTimeline',
          'onMenuShareAppMessage',
          'onMenuShareQQ',
          'onMenuShareWeibo',
          'hideMenuItems',
          'showMenuItems',
          'hideAllNonBaseMenuItem',
          'showAllNonBaseMenuItem',
          'chooseImage',
          'previewImage',
          'uploadImage',
          'downloadImage',
          'getNetworkType',
          'openLocation',
          'getLocation',
          'closeWindow',
          'scanQRCode',
          'chooseWXPay',
        ],
      }
    )
  },
  registerShare: function (info, callbak) {
    if (this.openInWeixin()) {
      var obj = Object.assign({
        fail: function () {
          window.alert('分享失' +
            '败，不要紧，可能是网络问题，一会儿再试试？')
        },
        success: function () {
          // 用户确认分享后执行的回调函数
        },
      }, info)

      this.ready(function () {
        wx.onMenuShareAppMessage(assign({}, obj, {
          success: function () {
            callbak && callbak()
            // track(`${trackCode}_AppMessage_share`)
          },
        }))
        wx.onMenuShareQQ(assign({}, obj, {
          success: function () {
            callbak && callbak()
            // track(`${trackCode}_QQ_share`)
          },
        }))
        wx.onMenuShareWeibo(assign({}, obj, {
          success: function () {
            callbak && callbak()
            // track(`${trackCode}_WeiBo_share`)
          },
        }))
        wx.onMenuShareTimeline(assign({}, obj, {
          title: obj.fcdesc || obj.desc,
          success: function () {
            callbak && callbak()
            // track(`${trackCode}_Timeline_share`)
          },
        }))
      })
    }
  },
  hideSharebutton: function () {
    wx.hideMenuItems({
      menuList: [
        'menuItem:share:appMessage',
        'menuItem:share:timeline',
        'menuItem:share:qq',
        'menuItem:share:weiboApp',
        'menuItem:share:facebook',
      ],
    })
  },
  showSharebutton: function () {
    wx.showMenuItems({
      menuList: [
        'menuItem:share:appMessage',
        'menuItem:share:timeline',
        'menuItem:share:qq',
        'menuItem:share:weiboApp',
        'menuItem:share:facebook',
      ],
    })
  },
  previewImage: function (imageList, index = 0) {
    wx.previewImage({
      current: imageList[index],
      urls: imageList,
    })
  },
}
