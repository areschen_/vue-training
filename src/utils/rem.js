let ratio

const setBaseFontSize = function () {
  var innerWidth = document.body.clientWidth
  ratio = innerWidth / 414
  document.getElementsByTagName('html')[0].style.fontSize = innerWidth / 10 + 'px'
}

export const getRemRatio = () => {
  return ratio
}

export const getRem = ($px) => {
  var $rem = 41.4
  console.log($px, $px / $rem)
  return $px / $rem
}

export const start = () => {
  document.addEventListener('DOMContentLoaded', function (e) {
    setBaseFontSize()
  }, false)
}
