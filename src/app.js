import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './icon.font'
import router from './router'
import './resource'
import { start } from './utils/rem'

console.log('__DEBUG__', __DEBUG__)
console.log('NODE_ENV', process.env.NODE_ENV)

start()
const app = new Vue({
  router,
  store,
  ...App,
})

app.$mount('#app')
