##条件
1. 每件商品均为100块
2. 输入商品编码，如果已经在购物车内，这购物车内数量加1，否则加入购物车   
3. 可以对购物车内商品加减数量
4. 计算购物车内总价

##分支
1. workshop-1  完成上述功能
2. workshop-2  购物车商品展示模块提出组件
3. workshop-3  添加商品模块提出组件
4. workshop-4  使用vuex完成功能
5. workshop-5  单元测试
6. workshop-6  路由练习

## tech
* vue2
* vue-router2
* vuex2
* webpack2

## setup
```
npm install or  yarn install
```

## watch and autobuild
```
npm run dev
```

## build

build for uat
```
NODE_ENV=uat npm run build
```
build for production
```
npm run build
```


## Module

```js

// store/modules/ui.js

const state = {
	shouldShowButton : true,
	progress : 0
};

const getters = {
	progress: state => state.progress
}

const setProgress = actionCreator('SET_PROGRESS',function({ commit }){

	const actionName = this.successActionName;

	return new Promise((resolve,reject)=>{
		window.setTimeout(()=>{
			resolve(40)
		},1000);
		window.setTimeout(()=>{
			commit(actionName,60)
		},2000)
		window.setTimeout(()=>{
			commit(actionName,100)
		},3000)
	})
});

export const actions = {
	setProgress
};

export const mutations = mutationCreator((on)=>{

	on(setProgress,(state,sssss)=>{
		state.progress = sssss
	});

	on.success(setProgress,(state,sssss)=>{
		console.log('setProgress success',sssss)
		state.progress = sssss
	});

});

export default {
	state,
	getters,
	actions,
	mutations
}

```