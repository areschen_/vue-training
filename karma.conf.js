var webpackConfig = require('./build/webpack.base.config.babel');

var webpack = webpackConfig({
  env: process.env.NODE_ENV,
  release: false,
  debug: true
});

delete webpack.entry;
webpack.plugins = webpack.plugins.slice(0, 3);

module.exports = function (config) {

  config.set({
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    autoWatch: false,
    singleRun: true,
    reporters: ['spec', 'coverage'],
    files: [
      'src/**/*_spec.js'
    ],
    preprocessors: {
      'src/**/*_spec.js': ['webpack']
    },
    webpack: webpack,
    webpackMiddleware: {
      noInfo: true
    },
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    }
  })
}