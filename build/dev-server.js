var webpackConfig = require('./webpack.dev.config.babel');
var webpack = require('webpack');
var webpackDevServer = require('webpack-dev-server');
var getRootPath = require('./tool/path');
var config = require('./config');

var env = process.env.NODE_ENV || 'dev';
var envConfig = require(getRootPath('env', env));

var compiler = webpack(webpackConfig);

var server = new webpackDevServer(compiler, {
	hot: true,
	quiet: false,
	noInfo: false,
	stats: { colors: true },
	proxy: envConfig.server || {}
});

server.listen(config.port);