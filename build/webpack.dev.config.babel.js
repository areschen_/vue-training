process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

var webpack = require('webpack');
var qs = require('qs');
var webpackConfig = require('./webpack.base.config.babel');
var config = require('./config');
var getRootPath = require('./tool/path');

var baseConfig  = webpackConfig({
	env: process.env.NODE_ENV,
	release: false,
	debug: true,
	publicPath: '/'
});

var styleConfig = {
	sourceMap: false,
	modules: false,
	importLoaders: 1,
	localIdentName: '[path]--[local]--[hash:base64:5]'
};

baseConfig.entry.app.unshift(
	"webpack/hot/dev-server",
	`webpack-dev-server/client?http://0.0.0.0:${config.port}`
);

baseConfig.output.publicPath = '/';

baseConfig.module.loaders.unshift({
	test: /\.font\.js$/,
	loaders: ['style-loader', 'css-loader','postcss-loader','fontgen-loader']
});

baseConfig.module.loaders = baseConfig.module.loaders.concat(
	{
		test: /\.scss$/,
		loaders: ['style-loader',`css-loader?${qs.stringify(styleConfig)}`,'postcss-loader','sass-loader']
	}
);

baseConfig.plugins = baseConfig
	.plugins
	.concat([
		new webpack.HotModuleReplacementPlugin()
	]);



module.exports = baseConfig;