var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var StyleLintPlugin = require('stylelint-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var cssnano = require('cssnano')
var getRootPath = require('./tool/path')

module.exports = function(config) {
  var publicPath = config.publicPath || '/'

  return {

    cache: true,

    // The entry point
    entry: {
      vendor: [
        'vue',
        'vue-router',
        'vue-touch',
        'vuex',
      ],
      app: [
        getRootPath('src/app.js'),
      ],
    },

    devServer: {
      open: true
    },

    output: {
      path: getRootPath('dist'),
      filename: '[name]-[hash].js',
      publicPath: publicPath,
    },

    module: {
      loaders: [
        // {
        //   test: /\.vue/,
        //   loader: 'eslint-loader',
        //   exclude: /node_modules/,
        //   include: getRootPath('src'),
        //   enforce: 'pre',
        // },
        // {
        //   test: /\.js/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules|_spec\.js)/,
        //   include: getRootPath('src'),
        //   enforce: 'pre',
        // },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          enforce: 'post',
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          enforce: 'post',
        },
        {
          test: /\.html$/,
          loader: 'html-loader',
        },
        {
          test: /\.css$/,
          loaders: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(png|jpg|gif)$/,
          loaders: [{
            loader: 'url-loader',
            query: { limit: 1000 },
          }],
        },
        {
          // required for bootstrap icons
          test: /\.(woff|woff2)(\?(.*))?$/,
          loaders: [{
            loader: 'url-loader',
            query: {
              prefix: 'factorynts/',
              limit: 5000,
              mimetype: 'application/font-woff',
            },
          }],
        },
        {
          test: /\.ttf(\?(.*))?$/,
          loaders: [{
            loader: 'file-loader',
            query: { prefix: 'fonts/'},
          }],
        },
        {
          test: /\.eot(\?(.*))?$/,
          loaders: [{
            loader: 'file-loader',
            query: { prefix: 'fonts/' },
          }],
        },
        {
          test: /\.svg(\?(.*))?$/,
          loaders: [{
            loader: 'file-loader',
            query: { prefix: 'fonts/' },
          }],
        },
        {
          test: /\.otf(\?(.*))?$/,
          loaders: [{
            loader: 'file-loader',
            query: { prefix: 'fonts/' },
          }],
        },
        {
          test: /\.json$/,
          loader: 'json-loader',
        },
      ],
    },

    resolve: {
      alias: {
        base_modules: getRootPath('base_modules'),
        config: getRootPath('env', config.env || 'dev'),
      },
      extensions: [
        '.js',
        '.vue',
        '.scss',
      ],
    },

    node: {
      __filename: true,
    },

    plugins: [

      new webpack.LoaderOptionsPlugin({
        context: __dirname,
        singleRun: true,
        debug: config.debug,
        options: {
          output: {
            publicPath,
          },
          sassLoader: {
            includePaths: getRootPath('src/styles'),
            outputStyle: 'expanded',
          },
          context: publicPath,
          postcss: [
            cssnano({
              autoprefixer: {
                add: true,
                remove: true,
                browsers: [
                  'Android >= 4.0',
                  'iOS >= 6',
                ],
              },
              discardComments: {
                removeAll: true,
              },
              discardUnused: false,
              mergeIdents: false,
              reduceIdents: false,
              safe: true,
              sourcemap: true,
            }),
          ],
        },
      }),

      new webpack.DefinePlugin({
        'process.env.NODE_ENV': `'${process.env.NODE_ENV}'`,
        __DEBUG__: !config.release,
      }),

      new webpack.ContextReplacementPlugin(/.*$/, /a^/),

      new StyleLintPlugin({
        syntax: 'scss',
        failOnError: !!config.release,
        quiet: false,
      }),

      new ExtractTextPlugin({
        filename: '[name].[hash].css',
        allChunks: true,
      }),

      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: '[name]-[hash].js',
      }),

      new HtmlWebpackPlugin({
        filename: './index.html',
        template: getRootPath('src/index.ejs'),
        inject: true,
        hash: true,
        // favicon: getRootPath('src/images/favicon.ico'),
        chunks: ['vendor', 'app'],
      }),
    ],
    devtool: 'source-map',
  }
}
