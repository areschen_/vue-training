var path = require('path');

module.exports  = function(){
	var basePath = [__dirname, '../../'];
	var args = Array.prototype.slice.call(arguments);
	return path.join.apply(
		null,basePath.concat(args)
	)
};