process.env.NODE_ENV = process.env.NODE_ENV || 'production';

var webpack = require('webpack');
var qs = require('qs');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpackConfig = require('./webpack.base.config.babel');
var config = require('./config');
var getRootPath = require('./tool/path');


var baseConfig  = webpackConfig({
	env : process.env.NODE_ENV,
	release : true,
	debug : false,
	publicPath: '/'
});

var styleConfig = {
	sourceMap: true,
	modules: false,
	importLoaders: 1,
	localIdentName: '[hash:base64:5]'
};

baseConfig.devtool = false;

baseConfig.module.loaders.unshift({
	test: /\.font\.js/,
	loader: ExtractTextPlugin.extract({
		fallbackLoader: 'style-loader',
		loader: 'css-loader!postcss-loader!fontgen-loader'
	})
});

baseConfig.module.loaders = baseConfig.module.loaders.concat(
	{
		test: /\.scss$/,
		loader: ExtractTextPlugin.extract({
			fallbackLoader: 'style-loader',
			loader: `css-loader?${qs.stringify(styleConfig)}!postcss-loader!sass-loader`
		})
	}
);

baseConfig.plugins = baseConfig.plugins.concat(
	new webpack.optimize.DedupePlugin(),
	new webpack.optimize.UglifyJsPlugin()
);

module.exports = baseConfig;